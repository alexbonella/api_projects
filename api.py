"""

************************************************************
Autor = @lexbonella                                        *  
Fecha = '31/01/2020'                                       *     
Nombre = API en Flask para consultar resultados de loteria * 
************************************************************ 

"""

from flask import *
import pandas as pd
import requests
from bs4 import BeautifulSoup
import json
import pymongo
from pymongo import MongoClient
import os 
from bson import json_util, ObjectId
import json

"""Inicializamos nuestro cliente mongodb"""
client = MongoClient('mongodb://localhost:27017/')

"""Base de datos"""
mydb = client["db_loterias"] # Database name
mycoll=mydb["resultados"] # Colecction name 

app = Flask(__name__)
 
@app.route("/")
def hello():
    return jsonify({'mesage':'Construyendo API'})

"""Consulta de resultados por fecha """
@app.route('/resultados/date=<string:dia>/<string:mes>/<string:ano>',methods=['GET'])
def get_resultados(dia,mes,ano):


    """"Realizamos query"""
    query=mycoll.find()
    date=str(dia)+'/'+str(mes)+'/'+str(ano)
    datos=[]
    
     # Consultamos resultados por fecha 
    for query in mycoll.find({'Fecha': date}):
        datos.append(query)

    response = json.loads(json_util.dumps(datos))
    return jsonify(response)        


if __name__ == '__main__':
    app.run(debug=True,port=4000)



